#include <iostream>
#include <string>
#include <iterator>
#include "Nucleus.h"


void Gene::init(const unsigned int start, const unsigned int end, const bool _on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = _on_complementary_dna_strand;
}
unsigned int Gene::get_start() const
{
	return this->_start;
}
unsigned int Gene::get_end() const
{
	return this->_end;
}
bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}
void Gene::set_start(const unsigned int start)
{
	this->_start = start;
}
void Gene::set_end(const unsigned int end)
{
	this->_end = end;
}
void Gene::set_on_complementary_dna_strand(const bool _on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = _on_complementary_dna_strand;
}


std::string Nucleus::get_complementary_DNA_strand()const
{
	return this->_complementary_DNA_strand;
}

void Nucleus::init(const std::string dna_sequence)
{
	std::string temp = "";
	if (!check_DNA(dna_sequence))
	{
		std::cerr << "dna sequence is not valid" << std::endl;
		_exit(1);
	}
	this->_DNA_strand = dna_sequence;
	for (int i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] == 'G')
		{
			temp += 'C';
		}
		else if (dna_sequence[i] == 'C')
		{
			temp += 'G';
		} 
		else if (dna_sequence[i] == 'T')
		{
			temp += 'A';
		}
		else
		{
			temp += 'T';
		}
	}
	this->_complementary_DNA_strand = temp;
}

bool Nucleus::check_DNA(const std::string dna_sequence)const
{
	for (int i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] != 'G' && dna_sequence[i] != 'C' && dna_sequence[i] != 'T' && dna_sequence[i] != 'A')
		{
			return false;
		}
	}
	return true;
}


std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string RNA = "";
	if (gene.is_on_complementary_dna_strand())
	{
		RNA = this->_complementary_DNA_strand.substr(gene.get_start(), gene.get_end());
	}
	else
	{
		RNA = this->_DNA_strand.substr(gene.get_start(), gene.get_end());
	}
	for (int i = 0; i < RNA.length(); i++)
	{
		if (RNA[i] == 'T')
		{
			RNA[i] = 'U';
		}
	}
	
	return RNA;
}


std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string str = "";
	for (int i = this->_DNA_strand.length() - 1; i >= 0; i++)
	{
		str += this->_DNA_strand[i];
	}
	return str;
}


unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned int count = 0;
	bool flag = true;
	while (flag)
	{
		if (this->_DNA_strand.find_first_of(codon) != std::string::npos)
		{
			count++;
		}
		else
		{
			flag = false;
		}
	}
	return count;
}