#pragma once
class Mitochondrion
{
private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
public:
	void init();
	void insert_glucose_receptor(const Protein& protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const;
	Protein* make_glocuse()const;
};